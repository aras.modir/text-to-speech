package com.aras.modir.texttospeech;

import android.speech.tts.TextToSpeech;
import android.widget.SeekBar;

public class PublicMethods {
    private void speak(String text, SeekBar mSeekBarPitch, SeekBar mSeekBarSpeed, TextToSpeech mTTS) {
        float pitch  = (float) mSeekBarPitch.getProgress() / 50;
        if (pitch < 0.1 ) pitch = 0.1f;
        float speed  = (float) mSeekBarSpeed.getProgress() / 50;
        if (speed < 0.1 ) speed = 0.1f;

        mTTS.setPitch(pitch);
        mTTS.setSpeechRate(speed);
        mTTS.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }
}
